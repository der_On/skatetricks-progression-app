#!/bin/bash

mkdir -p dist && \
npm run build && \
rsync -r --exclude .domains public/ dist/ && \
cd dist &&
git add --all && \
git commit -m'publish' && \
git push origin pages
