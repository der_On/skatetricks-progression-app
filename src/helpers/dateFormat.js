const dateFormat = new Intl.DateTimeFormat('de-DE', {
  weekday: 'short',
  year: 'numeric',
  month: '2-digit',
  day: '2-digit'
});

export default dateFormat;
