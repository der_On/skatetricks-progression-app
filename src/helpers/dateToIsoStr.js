import { padStart } from 'lodash';

function dateToIsoStr(date) {
  return [
    date.getFullYear(),
    padStart((date.getMonth() + 1) + '', 2, '0'),
    padStart(date.getDate() + '', 2, '0'),
  ].join('-');
}

export default dateToIsoStr;
