import { assign, repeat } from 'lodash';
import { v4 } from 'uuid';

function Skatespot(data = {}) {
  const skatespot = assign({
    id: v4(),
    title: null,
    description: '',
    lat: null,
    lng: null,
    images: [],
  }, data);

  skatespot.getGeoUrl = function () {
    return `geo:${skatespot.lat},${skatespot.lng}`;
  };

  skatespot.getOsmUrl = function () {
    return `https://www.openstreetmap.org/?mlat=${skatespot.lat}&mlon=${skatespot.lng}#map=19/${skatespot.lat}/${skatespot.lng}`;
  };

  skatespot.asText = function () {
    return `
${skatespot.title}
${repeat('=', skatespot.title - 1)}

Latitude: ${skatespot.lat}
Longitude: ${skatespot.lng}
Geo-Link: ${skatespot.getGeoUrl()}
Openstreetmap-Link: ${skatespot.getOsmUrl()}

Description
-----------

${skatespot.description.trim()}
    `.trim();
  };

  skatespot.asHtml = function () {
    return `
<h2>${skatespot.title}</h2>
<p>
  <strong>Latitude:</strong> ${skatespot.lat}<br/>
  <strong>Longitude:</strong> ${skatespot.lng}<br/>
  <a href="${skatespot.getGeoUrl()}">Geo-Link</a><br/>
  <a href="${skatespot.getOsmUrl()}">Openstreetmap-Link</a>
</p>
<h3>Description</h3>
<pre>
  ${skatespot.description.trim()}
</pre>
    `.trim();
  };

  skatespot.asJson = function () {
    return {
      id: skatespot.id,
      title: skatespot.title,
      description: skatespot.description,
      lat: skatespot.lat,
      lng: skatespot.lng,
      images: skatespot.images,
    };
  };

  return skatespot;
}

export default Skatespot;
