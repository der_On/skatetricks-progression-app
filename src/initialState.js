export default {
  search: '',
  tricks: [],
  trickGroups: [],
  skatespots: [],
  version: '0.0.0',
  settings: {
    darkMode: false,
    tricksAreCompact: false,
  },
  flashMessages: [],
  whatsNew: '',
  knownSkatetricks: {
    tricks: [],
    stances: [],
  },
};
