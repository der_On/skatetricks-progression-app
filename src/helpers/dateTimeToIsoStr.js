import dateToIsoStr from './dateToIsoStr.js';
import timeToIsoStr from './timeToIsoStr.js';

function dateTimeToIsoStr(date) {
  return [
    dateToIsoStr(date),
    timeToIsoStr(date)
  ].join(' ');
}

export default dateTimeToIsoStr;
