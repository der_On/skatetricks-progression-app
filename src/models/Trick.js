import { assign, repeat } from 'lodash';
import { v4 } from 'uuid';

function Trick(data = {}) {
  const trick = assign({
    id: v4(),
    name: null,
    stance: null,
    progress: 0,
    links: [],
    notes: '',
    isExpanded: false,
    sortOrder: 0,
    groupId: null,
  }, data);

  trick.asText = function () {
    const fullName = `${trick.stance === 'Regular' ? '' : trick.stance} ${trick.name}`;
    return `
${fullName}
${repeat('=', fullName.length - 1)}

Progress: ${Math.round(trick.progress * 100)}%

Links
-----

${trick.links.join('\n')}

Notes
-----

${trick.notes}
    `.trim();
  };

  trick.asHtml = function () {
    return `
<h2>${trick.stance === 'Regular' ? '' : trick.stance} ${trick.name}</h2>
<p>
  <strong>Progress:</strong> ${Math.round(trick.progress * 100)}%
</p>
<h3>Links</h3>
<ul>
  ${trick.links.map(link => {
    return `<li><a href="${link}">${link}</a></li>`;
  }).join('\n')}
</ul>

<h3>Notes</h3>

<p style="white-space: pre-line;">${trick.notes}</p>
    `.trim();
  };

  trick.asJson = function () {
    return {
      id: trick.id,
      name: trick.name,
      stance: trick.stance,
      progress: trick.progress,
      links: trick.links,
      notes: trick.notes,
      sortOrder: trick.sortOrder,
      groupId: trick.groupId,
    };
  };

  return trick;
}

export default Trick;
