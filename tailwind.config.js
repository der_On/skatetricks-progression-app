module.exports = {
  content: [
    'src/**/*.js',
    'src/**/*.svelte',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
  ],
}
