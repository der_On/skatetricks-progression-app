import App from './App.svelte';
import migrate from './migrate.js';

migrate();

const app = new App({
	target: document.body,
	props: {},
});

export default app;
